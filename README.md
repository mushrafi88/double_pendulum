# double_pendulum
a simulation of how double_pendulum behaves when connected together under large scale angles using gnuplot

to run this code
```
apt-get install gnuplot
```
for arch users
```
pacman -S gnuplot
```
```
git clone https://gitlab.com/mushrafi88/double_pendulum.git
cd double_pendulum
gnuplot double_pendulum.gnu
gnuplot lissajous_figure_for_double_pendulum.gnu
```

![](double_pendulum.mp4)

![](lissajous_motion.mp4)
